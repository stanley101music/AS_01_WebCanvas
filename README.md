# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


## FUNTCTION:
1. mousedown(): detect when mouse is down
    event.offsetX: x position of mouse
    event.offsetY: y position of mouse
    ctx.getImageData:  get the image data and store it to make undo and redo
    ctx.strokeStyle: the color of line
    ctx.fillStyle:   the color inside line
    ctx.fillText:    text
    ctx.moveTo:      move line to the current mouse position 
    ctx.beginPath(): start
    if(first):       record the first time the mouse down
    if(writting):    text mode
2. mousemove(): detet when mouse is moving
    if(pencil): pencil mode
    if(Rainbow): Rainbow mode: change color while mousemove
    ctx.lineTo():   move line to current position
3. mouseup(): detect when mouse is up
    if(rec): draw rectangle (use ctx.strokeRect())
    else if(cir): draw circle(use ctx.arc())
    else if(tri): draw triangle(use lineTo to locate three vertex)
    ctx.fill(): draw inside
    ctx.closePath(): end
4. changeColor(): change color
5. changeSize(): change size of brush
6. pen(): pen mode 
7. eraser(): eraser mode
8. rectangle(): rectangle mode
9. c(): circle mode
10. triangle(): triangle mode
11. w(): text mode
12. rainbow(): rainbow mode
13. Clear(): reload 
14. Download(): .toDataURL(): download img
15. Upload():   read file and onload use drawImage() to draw and img.src=event.target.result and readAsDaraURL to find the image
16.  Undo(): putImageData to put the last Imagedata
17.  Redo(): putImageData to put the next Imagedata